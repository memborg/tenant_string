extern crate wasm_bindgen;
use wasm_bindgen::prelude::*;

const SEPARATOR: char = '-';
const PADDING: char = '0';


#[wasm_bindgen]
pub fn trimmer(ts: &str) -> String {
    let parsed = parse(String::from(ts));
    let left_trimmed = trim_left(parsed.unwrap());
    concat(left_trimmed)
}

#[wasm_bindgen]
pub fn padder(ts: &str, com: i32, dep: i32, lea: i32, ten : i32) -> String {
    let parsed = parse(String::from(ts));

    let mut paddings:[i32; 4] = [0; 4];
    paddings[0] = com;
    paddings[1] = dep;
    paddings[2] = lea;
    paddings[3] = ten;

    let left_padded = pad_left(parsed.unwrap(), &paddings);
    concat(left_padded)
}

fn concat(sections: Vec<String>) -> String {
    sections.join(&SEPARATOR.to_string().to_string())
}

fn pad_left(sections: Vec<String>, paddings: &[i32]) -> Vec<String> {
    let mut secs: Vec<String> = vec![];
    for (i, item) in sections.iter().enumerate() {
        secs.push(left_pad_char(item.as_str(), paddings[i] as usize, PADDING));
    }

    secs
}

pub fn left_pad_char(s: &str, pad: usize, padchar: char) -> String
{
    let mut out = String::new();

    let len = s.len();
    if pad > len {
        for _ in 0..pad-len {
            out.push(padchar);
        }
    }
    out.push_str(s);

    out
}

fn trim_left(sections: Vec<String>) -> Vec<String> {
    let secs = sections.iter().map(|s| {
        s.as_str().trim_left_matches(PADDING).to_string()
    }).collect::<Vec<_>>();

    secs
}

fn parse(string: String) -> Result<Vec<String>, &'static str> {
    let mut sections: Vec<String> = vec![];

    let mut it = string.chars().peekable();
    let mut sec = String::new();

    loop {
        match it.peek() {
            Some(&ch) => match ch {
                '0' ... '9' => {
                    sec.push(ch);
                    it.next().unwrap();
                },
                SEPARATOR => {
                    sections.push(sec);
                    sec = String::new();
                    it.next().unwrap();
                },
                _ => return Err("invalid char")
            },
            None => break
        }
    }

    if !sec.is_empty() {
        sections.push(sec);
    }

    Ok(sections)
}

