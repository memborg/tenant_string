const rust = import("./tenant_string");

function getRandomIntBetween(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomTenantString() {
  var str = [];
  str.push(getRandomIntBetween(1, 99));
  str.push(getRandomIntBetween(1, 999));
  str.push(getRandomIntBetween(1, 99));
  str.push(getRandomIntBetween(1, 99));

  return str.join("-");
}

function getAvg(arr) {
  var sum = arr.reduce(function(a, b) {
    return a + b;
  });
  return sum / arr.length;
}

function init() {
  var tenants = [];
  var avgTimes = [];
  var counts = 1000000;

  console.log("Generate strings");
  for (var i = 0; i < counts; i++) {
    tenants.push(getRandomTenantString());
  }

  rust.then(m => {
    let res = m.trimmer("01-1019");

    console.time("wasm.tenant_string");
    tenants.forEach(function(tenant) {
      var s = new Date();
      let res2 = m.padder(tenant, 3, 4, 3, 2);
      var e = new Date();

      avgTimes.push(e.getTime() - s.getTime());
    });
    console.timeEnd("wasm.tenant_string");

    var avg = getAvg(avgTimes);

    console.log("WASM average", avg, "millisecond");
  });
}

init();
