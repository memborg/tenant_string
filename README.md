# README

## Prerequisites

### Rust nightly

Install rust from <https://www.rust-lang.org/en-US/>

    rustup install nightly
    rustup target add wasm32-unknown-unknown --toolchain nightly

### Node

Install node

    npm install -g webpack

## WASM bindgen-cli

`cargo install wasm-bindgen-cli`

# Building

    cargo +nightly build --target wasm32-unknown-unknown
    wasm-bindgen target/wasm32-unknown-unknown/debug/wasm_greet.wasm --out-dir .
    
# Running

    npm run serve
    
Point browser to <localhost:8080>

Watch `console.log` entries